# Tourit.js - Give your users a tour of your web app

A lot of times devs want to give their users tours of their application so that the user has a better idea of how to use it. This is a small, zero-dependency script that allows you to do just that. The javascript is about 1.4 kb when minified and gzipped.

[See Demo Here](https://andrewfulrich.gitlab.io/tourit.js/)

## To use:

1. include the script and stylesheet
```html
<link rel="stylesheet" href="tourit.css" />
<script src="tourit.js"></script>
```
2. Add `data-tourit` and `data-tourit-next` attributes to all the stops on your tour. The `data-tourit` is the id of a corresponding html template that holds the description for this stop on the tour, and `data-tourit-next` is the id of the next stop in the tour. If one of the stops in your tour is itself a template tag, then it'll just use the content of that for the description and place it in the middle of the screen.
```html
  <div 
    id="first" 
    data-tourit="firstTooltip" 
    data-tourit-next="second"
  >
    Really Cool Thing
  </div>
  <template id="firstTooltip">
    This is the first thing I'd like to show you
  </template>
  <div 
    id="second" 
    data-tourit="secondTooltip"
    data-tourit-next="third"
  >
    Another cool thing
  </div>
  <template id="secondTooltip">
    <h2>Wow!</h2>
    <p>I can style stuff using the full power of html and css!</p>
  </template>
  <template id="third">
    <p>I hope you enjoyed the tour!</p>
  </template>
```
3. make the tour!
```javascript
const doTheTour= tourit('first')
doTheTour()
```

## To style:

The current css sheet is minimal so it shouldn't get in the way too much and overriding or modifying it is a good place to start. Just note that modifying it could mean you have to think harder about upgrades, but since it's so small, maybe not too hard. You basically have a back button, forward button, tooltip window, and highlight for highlighting the current element, and they all have ids you can use in your css selectors (see tourit.css or use your dev tools).

## Current Known Issue

If users scroll around on the final tour stop, they might skip to a prior stop. Pull requests are welcome. There is a half-hearted attempt to avoid this bug by preventing user-initiated scrolling during the tour, but a [full-strength attempt](https://github.com/willmcpo/body-scroll-lock/blob/master/src/bodyScrollLock.js) weighs about 8 times as much as the entire library. 