/**
 * tourit.js Copyright (c) 2020 Andrew Ulrich under MIT licenase
 * start a tour of the web page
 * startId - the id of the first element to start with
 * introText (optional) - some intro text to show before going to the first element
 */
function tourit(startId) {
  function makeEl(id) {
    const el=document.createElement('div')
    el.setAttribute('id',id)
    el.classList.add('tourit-hide')
    document.body.appendChild(el)
    return el
  }
  function makeNavButton(label,parent) {
    const btn = document.createElement('button')
    btn.textContent=label
    btn.onclick=e=>{
      if(btn.clickId) getNext(btn.clickId)
    }
    btn.hideOrShow=()=>{
      if(btn.clickId !== null && btn.clickId !== undefined) {
        btn.classList.remove('tourit-hide')
        btn.disabled=false
      } 
      else btn.classList.add('tourit-hide')
    }
    btn.classList.add('tourit-hide')
    parent.appendChild(btn)
    return btn
  }

  const bg=makeEl('tourit-bg')
  bg.onwheel=e=>{
    e.stopPropagation();
    e.preventDefault();
  }
  const win=makeEl('tourit-window')
  const tooltip=makeEl('tourit-tooltip')
  
  function cancel() {
    [tooltip,win,bg].forEach(el=>el.classList.add('tourit-hide'))
  }
  
  const cancelSpan=document.createElement('span')
  cancelSpan.classList.add('tourit-cancel-span')
  const cancelBtn=document.createElement('button')
  cancelBtn.onclick=cancel
  bg.onclick=cancel
  cancelBtn.textContent='X'
  cancelSpan.appendChild(cancelBtn)
  tooltip.appendChild(cancelSpan)

  const tooltipSpan=document.createElement('span')
  tooltipSpan.classList.add('tourit-tooltip-text')
  tooltip.appendChild(tooltipSpan)
  const prevBtn=makeNavButton('< Previous',tooltip)
  const nextBtn=makeNavButton('Next >',tooltip)
  //keyboard navigation: arrows to move, escape to close
  function keyUp(e) {
    if(!Array.from(bg.classList).includes('tourit-hide')) {
      if([39,13,32].includes(e.keyCode)
      && (nextBtn.clickId !== null || nextBtn.clickId !== undefined)) {
        nextBtn.dispatchEvent(new CustomEvent('click'))
      } else if([37,8].includes(e.keyCode) 
      && (prevBtn.clickId !== null || prevBtn.clickId !== undefined)) {
        prevBtn.dispatchEvent(new CustomEvent('click'))
      } else if(e.keyCode == 27) {
        cancel()
      }
    }
    
  }
  document.addEventListener('keyup',keyUp)

  let bgDims = bg.getBoundingClientRect()
  let timeoutId;
  //used to call a callback after scrolling has stopped. If scroll is called again, the timeout's interrupted and reset
  function interruptedFunc(cb) {
    return ()=>{
      clearTimeout(timeoutId);
      timeoutId = setTimeout( ()=>{
        window.removeEventListener('scroll',window.interruptedFunc)
        cb()
      } , 150 );
    }
  }

  function getNext(id) {
    bg.classList.remove('tourit-hide')
    bgDims = bg.getBoundingClientRect()
    
    const el=document.getElementById(id)
    if(id !== startId) {
      const prevEl=document.querySelector(`[data-tourit-next=${id}]`)
      prevBtn.clickId=prevEl? prevEl.getAttribute('id') : null
    } else prevBtn.clickId=null
    
    nextBtn.clickId=el.dataset['touritNext']
    prevBtn.disabled=true
    nextBtn.disabled=true
    
    if(el.tagName=='TEMPLATE') {
      bg.style.clipPath='none'
      prevBtn.hideOrShow()
      nextBtn.hideOrShow()
      win.classList.add('tourit-hide')
      showToolTipWithContent(
        tDims=>bgDims.width/2-tDims.width/2,
        tDims=>bgDims.height/2-tDims.height/2,
        el.innerHTML
        )
    } else {
      let elDims=el.getBoundingClientRect()

        //if el outside window view, scrollto and then get the new bgDims since it's position:fixed
      if(elDims.x < 0 || elDims.y < 0 || elDims.x > bgDims.width || elDims.y > bgDims.height) {
        window.interruptedFunc=interruptedFunc(render)
        window.addEventListener('scroll',window.interruptedFunc);
        el.scrollIntoView({behavior:'smooth'}) 
      } else render()
      function render() {
        bgDims=bg.getBoundingClientRect()
        elDims=el.getBoundingClientRect()
        prevBtn.hideOrShow()
        nextBtn.hideOrShow()
        bg.style.clipPath=`polygon(0px 0px ,0px ${bgDims.height}px, ${bgDims.width}px ${bgDims.height}px, ${bgDims.width}px 0px, 0px 0px, ${elDims.x}px ${elDims.y}px, ${elDims.x+elDims.width}px ${elDims.y}px, ${elDims.x+elDims.width}px ${elDims.y+elDims.height}px, ${elDims.x}px ${elDims.y+elDims.height}px, ${elDims.x}px ${elDims.y}px)`
        win.style.top=elDims.y+window.scrollY+'px'
        win.style.left=elDims.x+window.scrollX+'px'
        win.style.width=elDims.width+'px'
        win.style.height=elDims.height+'px'
        win.classList.remove('tourit-hide')
    
        let content=''
        let isText=true
        if(el.dataset['tourit'] && document.getElementById(el.dataset['tourit'])) {
          content=document.getElementById(el.dataset['tourit']).innerHTML
          isText=false
        }
        const margin=5
        showToolTipWithContent(
          tDims=>(tDims.width> elDims.width? Math.max(bgDims.x+margin,elDims.right-tDims.width) : elDims.x)+window.scrollX,
          tDims=>(elDims.bottom+tDims.height+margin > bgDims.bottom ? elDims.y-tDims.height-margin : elDims.bottom+margin)+window.scrollY,
          content,
          isText
        )
      }
    }

    
  }
  function showToolTipWithContent(positionXCallback,positionYCallback,content,isText=false) {
    tooltip.classList.remove('tourit-hide')
    if(isText) tooltipSpan.textContent=content
    else tooltipSpan.innerHTML=content
    function positionIt() {
      const tDims=tooltip.getBoundingClientRect()
      tooltip.style.left=positionXCallback(tDims)+'px'
      tooltip.style.top=positionYCallback(tDims)+'px'
    }
    positionIt()
    Promise.all(Array.from(tooltip.querySelectorAll('img')).map(img=>new Promise(resolve=>img.onload=resolve))).then(()=>{
      positionIt()
    })
  }
  return ()=>getNext(startId)
}
  